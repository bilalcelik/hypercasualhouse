using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    GameManager gameManager;
    public List<Level> levelList;

    public bool isRandomLevel;

    private Transform levelParent;

    void Awake()
    {
        gameManager = GetComponent<GameManager>();
        GameObject parent = new GameObject();
        parent.name = "[LEVEL]";
        parent.transform.parent = null;
        levelParent = parent.transform;
    }
    public void CreateLevel()
    {
        int level = gameManager.GetCurrentLevel()-1;

        level = level % levelList.Count;
        foreach(Transform item in levelParent)
        {
            Destroy(item.gameObject);
        }
        Instantiate(levelList[level].level,levelParent);


       
    }


}
