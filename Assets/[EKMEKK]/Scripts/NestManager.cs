using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NestManager : MonoBehaviour
{
    public static NestManager instance;

    public Nest[] nests;

    //[HideInInspector]
    public Nest selectedNest;
    //[HideInInspector]
    public int currentNestIndex = 0;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        currentNestIndex = 1;
        selectedNest = nests[0];

    }

    public void NextNest()
    {
        if (currentNestIndex >= nests.Length)
        {
            Debug.LogError("leveli bitir");

            GameManager2.instance.Success();
        }
        else
        {
            selectedNest = nests[currentNestIndex];
            selectedNest.ActivateNest();


            currentNestIndex++;

        }



    }
}
