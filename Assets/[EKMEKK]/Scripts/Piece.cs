using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Piece : MonoBehaviour
{
    public PieceType pieceType;

    public Nest myNest;
    bool isDone = false;

    public bool isMultipleChild = false;
    MeshRenderer[] childMeshes;
    MeshRenderer meshRenderer;

    PieceGroup pg;
    NestController nc;
    BoxCollider collider;

    Vector3 startPos;

    Rigidbody rb;

    void Start()
    {
        nc = FindObjectOfType<NestController>();
        pg = GetComponentInParent<PieceGroup>();
        collider = GetComponent<BoxCollider>();

        if (!isMultipleChild)
        {
            meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.enabled = false;
        }
        else
        {
            childMeshes = GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < childMeshes.Length; i++)
            {
                childMeshes[i].enabled = false;
            }
        }

        collider.enabled = false;

        startPos = transform.position;

        rb = GetComponent<Rigidbody>();

    }

    void OnMouseDown()
    {
        if (GameManager2.instance.isStart && !isDone)
        {
            rb.isKinematic = true;

            Vector3 pos = Dragger.instance.GetPoint();

            transform.DOMove(pos, .05f).SetEase(Ease.Linear).OnComplete(() =>
            {
                if (!isDone)
                    isMove = true;
            });
        }

    }

    void OnMouseDrag()
    {

    }

    void OnMouseUp()
    {
        if (GameManager2.instance.isStart && !isDone)
        {
            isMove = false;

            rb.isKinematic = false;
        }

    }

    public void Activate()
    {
        rb.isKinematic = false;

        ChangeMeshView(true);
        collider.enabled = true;
        pg.pieceCountText.gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NestTrigger")
        {

            
            if(nc.selectedNest.pieceType == pieceType)
            {
                isDone = true;
                isMove = false;
                rb.isKinematic = true;
                nc.selectedNest.GetYourPiece(gameObject);
                pg.UpdateCount();
            }
            else if(myNest != null && myNest.isActive)
            {
                isDone = true;
                isMove = false;
                rb.isKinematic = true;
                myNest.GetYourPiece(gameObject);
                pg.UpdateCount();
            }

        }
    }


    void ChangeMeshView(bool gate)
    {
        if (!isMultipleChild)
        {
            meshRenderer.enabled = gate;
        }
        else
        {
            for (int i = 0; i < childMeshes.Length; i++)
            {
                childMeshes[i].enabled = gate;
            }
        }
    }

    public bool clampZ, clampX;
    bool isMove = false;

    private void LateUpdate()
    {
        if (isMove)
        {
            ClampPos();
        }
    }

    private void ClampPos()
    {
        Vector3 pos = Dragger.instance.GetPoint();

        if (pos.x < -10f)
        {
            clampX = true;
            pos.x = -10f;
            startPos.x = pos.x;
        }
        else if (pos.x > -5)
        {
            clampX = true;
            pos.x = -5;
            startPos.x = pos.x;
        }
        else
        {
            clampX = false;
        }

        if (pos.z < -2f)
        {
            clampZ = true;
            pos.z = -2f;
            startPos.z = pos.z;
        }
        else if (pos.z > 2.5f)
        {
            clampZ = true;
            pos.z = 2.5f;
            startPos.z = pos.z;
        }
        else
        {
            clampZ = false;
        }

        //pos.x = Mathf.Clamp(pos.x, -1.75f, 2f);
        //pos.z = Mathf.Clamp(pos.z, -2.25f, 1.5f);

        transform.position = pos;
    }

}

public enum PieceType
{
    woodDoor,
    woodWindow,
    woodWall,
    woodWallWindow,
    woodWallDoor,
    woodRoof,
}




#region surukleme

//float boundaryX, boundaryZ;

//public bool clampZ, clampX;

//private void LateUpdate()
//{
//    if (isMove)
//    {
//        ClampPos();
//    }
//}

//private void ClampPos()
//{
//    Vector3 pos = Dragger.instance.GetPoint();

//    if (pos.x < -2f)
//    {
//        clampX = true;
//        pos.x = -2f;
//        startPos.x = pos.x;
//    }
//    else if (pos.x > 2)
//    {
//        clampX = true;
//        pos.x = 2;
//        startPos.x = pos.x;
//    }
//    else
//    {
//        clampX = false;
//    }

//    if (pos.z < -2f)
//    {
//        clampZ = true;
//        pos.z = -2f;
//        startPos.z = pos.z;
//    }
//    else if (pos.z > 2f)
//    {
//        clampZ = true;
//        pos.z = 2f;
//        startPos.z = pos.z;
//    }
//    else
//    {
//        clampZ = false;
//    }

//    //pos.x = Mathf.Clamp(pos.x, -1.75f, 2f);
//    //pos.z = Mathf.Clamp(pos.z, -2.25f, 1.5f);

//    transform.position = pos;
//}

#endregion
