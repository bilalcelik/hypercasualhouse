using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NestController : MonoBehaviour
{
    public Nest[] nests;

    //[HideInInspector]
    public Nest selectedNest;
    //[HideInInspector]
    public int currentNestIndex = 0;

    [Space]
    public GameObject EndHousePos2;
    public GameObject EndHousePos3;

    [Space]
    public GameObject Roof;
    public GameObject RoofNest;

    [Space]
    public GameObject RoofExplosion;

    Camera camera;

    [Header("Flash")]
    public CanvasGroup myCG;
    private bool flash = false, isEnd = false;

    void Start()
    {

        if (nests.Length == 0)
        {
            nests = GetComponentsInChildren<Nest>();
        }

        currentNestIndex = 1;
        selectedNest = nests[0];

        camera = FindObjectOfType<Camera>();


    }



    void Update()
    {
        if (flash)
        {

            myCG.alpha = myCG.alpha - Time.deltaTime;
            if (myCG.alpha <= 0)
            {
                myCG.alpha = 0;
                flash = false;
                GameManager2.instance.Success();
            }
        }

        if (isEnd)
        {

            // TEK BASI�
            if (Input.GetMouseButtonDown(0))
            {
                isEnd = false;
                transform.DOKill();



                transform.DOLocalRotate(Vector3.zero, .3f).SetEase(Ease.Linear);

                transform.DOLocalMove(Vector3.zero, .3f).SetEase(Ease.InQuart);

                Roof.transform.parent = RoofNest.transform;
                Roof.transform.DOLocalRotate(Vector3.zero, 1.75f).SetEase(Ease.Linear);
                Roof.transform.DOLocalMove(Vector3.zero, 1.75f).SetEase(Ease.InQuart).OnComplete(() =>
                {

                    FlashEffect();
                    RoofExplosion.gameObject.SetActive(true);
                    //GameManager2.instance.Success();

                });
            }

        }




    }

    public void FlashEffect()
    {
        if (!flash)
        {
            flash = true;
            myCG.alpha = 1;
        }

        camera.DOShakePosition(.5f, .3f, 10, 90);
        camera.DOShakeRotation(.5f, .3f, 10, 90);
    }

    public void NextNest()
    {
        if (currentNestIndex >= nests.Length)
        {
            Debug.LogError("leveli bitir");

            GameManager2.instance.isEndGame = true;
            //GameManager2.instance.Success();
        }
        else
        {
            selectedNest = nests[currentNestIndex];
            selectedNest.ActivateNest();


            currentNestIndex++;

        }

    }

    public void EndHouse()
    {
        transform.parent = EndHousePos3.transform;

        // EV�N G�DECE�� X POZ�SYONUNA G�T�R�R
        transform.DOMoveX(EndHousePos3.transform.position.x, 1f).SetEase(Ease.Linear);

        // EV�N POL�NOM G�B� G�TMES� ���N GEREKL� Y DE�ER�
        transform.DOMoveY(EndHousePos2.transform.position.y, .5f).SetEase(Ease.OutCubic).SetLoops(2, LoopType.Yoyo);

        // ROTASYONUNU SIFIRLAR
        transform.DOLocalRotate(Vector3.zero, 1f).SetEase(Ease.Linear).OnComplete(() =>
        {
            // KULLANICI TEKRAR BASSIN D�YE A�A�I YUKARI HAREKET EDER
            transform.DOMoveY(EndHousePos3.transform.position.y + .5f, .5f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
            //transform.DOLocalRotate(new Vector3(0, 0, 360), 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);

            isEnd = true;

        });

    }
}
