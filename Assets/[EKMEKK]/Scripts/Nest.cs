using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Nest : MonoBehaviour
{

    //public Piece myPiece;
    public PieceType pieceType;

    public bool isActive = false;

    public bool isMultipleChild = false, isEndNest = false;
    MeshRenderer[] childMeshes;
    MeshRenderer meshRenderer;

    Vector3 startScale;
    NestController nc;

    // Start is called before the first frame update
    void Start()
    {
        nc = FindObjectOfType<NestController>();
        startScale = transform.localScale;

        if (!isMultipleChild)
        {
            meshRenderer = GetComponent<MeshRenderer>();

            if (!isActive)
                meshRenderer.enabled = false;
        }
        else
        {
            childMeshes = GetComponentsInChildren<MeshRenderer>();

            if (!isActive)
            {
                for (int i = 0; i < childMeshes.Length; i++)
                {
                    childMeshes[i].enabled = false;
                }
            }

        }

    }


    public void ActivateNest()
    {
        isActive = true;
        ChangeMeshView(true);


    }

    public void GetYourPiece(GameObject piece)
    {
        isActive = false;
        nc.NextNest();

        piece.transform.parent = transform;
        piece.transform.DOMove(transform.position, .5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            transform.DOKill();
            transform.localScale = startScale;
            ChangeMeshView(false);

            //nc.FlashEffect();
        });

        piece.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.Linear);
        piece.transform.DOLocalRotate(Vector3.zero, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            if (isEndNest)
            {
                FindObjectOfType<CamControl>().MoveHouse();
                nc.EndHouse();
            }
        });

    }

    void ChangeMeshView(bool gate)
    {
        if (!isMultipleChild)
        {
            meshRenderer.enabled = gate;
        }
        else
        {
            for (int i = 0; i < childMeshes.Length; i++)
            {
                childMeshes[i].enabled = gate;
            }
        }
    }
}
