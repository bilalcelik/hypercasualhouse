using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CamControl : MonoBehaviour
{
    [Header("Cam Positions")]
    public GameObject HouseCam;
    public GameObject EndCam, CarCam;
    [Space]
    public GameObject car;
    Camera camera;

    public Action DummyAction;
    NestController nc;
    PieceGroup[] pg;

    void Start()
    {
        camera = GetComponent<Camera>();
        nc = FindObjectOfType<NestController>();
        pg = FindObjectsOfType<PieceGroup>();

    }

    public void StartGame()
    {
        transform.parent = HouseCam.transform;

        nc.gameObject.SetActive(false);

        transform.DOLocalRotate(Vector3.zero, 1f).SetEase(Ease.Linear);                         // CAMERA ARACIN ORAYA ROTATE EDER
        DOTween.To(() => camera.fieldOfView, x => camera.fieldOfView = x, 50, 1f);              // CAMERANIN F�ELD OF V�EW'INI YAKLA�TIR

        transform.DOLocalMove(Vector3.zero, 1f).SetEase(Ease.Linear).OnComplete(() =>           // CAMERA ARACAN G�DER
        {

            car.transform.DOMoveZ(4.3f, 2f).SetEase(Ease.Linear).OnComplete(() =>               // ARA� EV�N �N�NE GEL�R
            {

                // R�MORKTAK� OBJELER�N K�NEMAT���N� A�AR
                Rigidbody[] rbs = car.transform.GetComponentsInChildren<Rigidbody>();
                for (int i = 0; i < rbs.Length; i++)
                {
                    rbs[i].isKinematic = false;
                }


                // R�MORK YUKARI KALKAR
                car.transform.GetChild(0).DOLocalRotate(new Vector3(350, car.transform.GetChild(0).localRotation.eulerAngles.y, car.transform.GetChild(0).localRotation.eulerAngles.z), 2f).SetEase(Ease.Linear).OnComplete(() =>
                {
                    // B�RAZ BEKLER VE KASADAK� PAR�ALAR YERLER�NE G�DER
                    StartCoroutine(WaitForFall());

                });

                // R�MORK GER� �NER
                car.transform.GetChild(1).DOLocalRotate(new Vector3(90, car.transform.GetChild(1).localRotation.eulerAngles.y, car.transform.GetChild(1).localRotation.eulerAngles.z), 1f).SetEase(Ease.Linear).OnComplete(() =>
                {
                });
            });

        });

    }

    IEnumerator WaitForFall()
    {
        yield return new WaitForSeconds(1f);


        // CAMERA EVE BAKMAYA GE�ER
        camera.transform.parent = HouseCam.transform;
        camera.transform.DOLocalRotate(Vector3.zero, 1f).SetEase(Ease.Linear);
        DOTween.To(() => camera.fieldOfView, x => camera.fieldOfView = x, 60, 1f);
        camera.transform.DOLocalMove(Vector3.zero, 1f).SetEase(Ease.Linear);



        Transform[] childs = new Transform[car.transform.GetChild(0).childCount];
        for (int i = 0; i < car.transform.GetChild(0).childCount; i++)
        {
            childs[i] = car.transform.GetChild(0).GetChild(i);
        }

        for (int j = 0; j < childs.Length; j++)
        {
            childs[j].parent = transform.root;
        }


        car.transform.GetChild(0).DOLocalRotate(new Vector3(270, car.transform.GetChild(0).localRotation.eulerAngles.y, car.transform.GetChild(0).localRotation.eulerAngles.z), 1f).SetEase(Ease.Linear).OnComplete(() =>
        {
            //StartCoroutine(WaitForFall());
            DummyAction.Invoke();


            nc.gameObject.SetActive(true);
            GameManager2.instance.isStart = true;


            car.transform.DOMoveZ(21f, 2f).SetEase(Ease.Linear).OnComplete(() =>
            {

            });
        });

    }


    public void MoveHouse()
    {
        transform.parent = EndCam.transform;

        transform.DOLocalRotate(Vector3.zero, 1f).SetEase(Ease.Linear);                         // CAMERA ARACIN ORAYA ROTATE EDER
        transform.DOLocalMove(Vector3.zero, 1f).SetEase(Ease.Linear);                           // CAMERA ARACAN G�DER
    }

}
