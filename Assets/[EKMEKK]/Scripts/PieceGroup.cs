using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PieceGroup : MonoBehaviour
{

    public Piece[] pieces;
    public TextMeshProUGUI pieceCountText;

    public int pieceCount;

    // Start is called before the first frame update
    void Start()
    {
        pieceCountText = GetComponentInChildren<TextMeshProUGUI>();
        pieces = GetComponentsInChildren<Piece>();
        
    }

    public void UpdateCount()
    {

        if(pieceCount == 0)
        {
            //pieceCountText.gameObject.SetActive(false);
        }
        else
        {
            // B�R SONRAK� G�T�R�LECEK OLAN PAR�ANIN LAYER'INI DEFAULT'A ALIYORUZ K� TIKLANDI�INDA ALGILASIN
            pieces[pieces.Length - pieceCount].gameObject.layer = 0;
        }
    }

}
