using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragger : MonoBehaviour
{
    public static Dragger instance;

    public GameObject planeCreatePos;

    //ProductController pc;
    //Player player;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        //player = GetComponent<Player>();
        //pc = FindObjectOfType<ProductController>();
    }

    private RaycastHit hit;

    public Vector3 GetPoint()
    {
        var fwd = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(transform.position, fwd * 100, Color.red);
        Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f);


        //Debug.Log(hit.collider);
        float distance;

        Plane plane = new Plane(Vector3.up, planeCreatePos.transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (plane.Raycast(ray, out distance))
        {
            //hit.collider.gameObject.transform.position = ray.GetPoint(distance);
            Vector3 pos = new Vector3(ray.GetPoint(distance).x, ray.GetPoint(distance).y + .1f, ray.GetPoint(distance).z);
            return pos;
        }
        else
        {
            return Vector3.zero;
        }

        //pc.SelectNest(hit.collider.gameObject);



    }
}
