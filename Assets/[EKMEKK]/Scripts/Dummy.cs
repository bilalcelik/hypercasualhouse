using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Dummy : MonoBehaviour
{
    public Piece originalPiece;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<CamControl>().DummyAction += FindYourOriginal;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FindYourOriginal()
    {

        transform.DOMove(originalPiece.transform.position, 1f).SetEase(Ease.Linear);


        transform.DORotate(originalPiece.transform.rotation.eulerAngles, 1f).SetEase(Ease.Linear).OnComplete(() =>
        {
            gameObject.SetActive(false);
            originalPiece.Activate();
        });
    }
}
