using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Subscribe(GameManager.GameState.GameStart, () =>
        {
            Debug.Log("Oyun Başladı");
        });
         GameManager.Instance.Subscribe(GameManager.GameState.Btn_ClickRetry, () =>
        {
            Debug.Log("Kaybettin tekrar oyna");
        });
         GameManager.Instance.Subscribe(GameManager.GameState.Btn_ClickContinue, () =>
        {
            Debug.Log("Kazandın yeni bölüm");
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            GameManager.Instance.Success();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameManager.Instance.Fail();
        }
    }
}
